use std::io;
use std::io::{BufRead, Write};

use matasano::exercises::set_one;

fn main() -> io::Result<()> {
    // I guess since I'm not multithreading anything I want the locked versions?
    // If I *do* add a thread, and try to do IO there, will there be an obvious indication?
    let mut stdin = io::stdin().lock();
    let mut stdout = io::stdout().lock();
    let mut input = String::new();

    loop {
        stdout
            .write_all(b"\nSet: (q to exit): ")
            .and(stdout.flush())?;
        input.clear();
        stdin.read_line(&mut input)?;
        // I think it was worth wasting a *little* time on std:io, but I should probably just get a crate for menus
        if input.starts_with("1") {
            set_one::select(&mut stdin, &mut stdout)?;
        } else if input.starts_with("q") {
            break;
        } else {
            stdout
                .write_all(b"\nI haven't done that set yet.\n")
                .and(stdout.flush())?;
        }
    }

    Ok(())
}
