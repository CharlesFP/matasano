use cached::proc_macro::cached;
use std::fs;
use std::io::Read;
use std::path;

pub fn xor(xs: Vec<u8>, ys: Vec<u8>) -> Result<Vec<u8>, String> {
    (xs.len() == ys.len())
        .then_some(xs.iter().zip(ys.iter()).map(|(&x, &y)| x ^ y).collect())
        .ok_or(String::from("Inputs should be the same length."))
}

pub fn calc_frequencies(xs: &Vec<u8>) -> [f32; 256] {
    let mut freqs: [u32; 256] = [0 as u32; 256];
    let num_chars: f32 = xs.len() as f32;
    for &x in xs {
        freqs[x as usize] += 1
    }
    freqs.map(|c| c as f32 / num_chars)
}

pub fn score_frequencies(freqs: [f32; 256], reference: [f32; 256]) -> f32 {
    freqs
        .zip(reference)
        .map(|(f, r)| (f - r).powi(2))
        .iter()
        .sum::<f32>()
        .sqrt()
}

#[cached]
pub fn reference_frequencies() -> [f32; 256] {
    let path_to_reference = path::Path::new(".")
        .join("resources")
        .join("tale_of_two_cities.txt");
    let mut reference_file =
        fs::File::open(&path_to_reference).expect("missing reference materials");
    let mut buf = String::new();
    reference_file
        .read_to_string(&mut buf)
        .expect("file system error I guess?");

    calc_frequencies(&buf.as_bytes().to_vec())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_xor() -> () {
        assert_eq!(xor(vec![], vec![]).unwrap(), vec![]);
        assert_eq!(xor(vec![6, 12], vec![3, 6]).unwrap(), vec![5, 10]);
        assert!(xor(vec![], vec![1]).is_err());
    }

    #[test]
    fn test_calc_frequencies() -> () {
        assert_eq!(calc_frequencies(&vec![1])[1], 1.0f32);
        assert_eq!(calc_frequencies(&vec![1, 2])[1], 0.5f32);
    }
    #[test]
    fn test_score_frequencies() -> () {
        let dumb_test_frequencies = calc_frequencies(&vec![1]);
        let dumb_test_frequencies_b = calc_frequencies(&vec![1, 2, 3, 4]);
        assert_eq!(score_frequencies([0f32; 256], [0f32; 256]), 0f32);
        assert_eq!(score_frequencies(dumb_test_frequencies, [0f32; 256]), 1f32);
        assert_eq!(
            score_frequencies(dumb_test_frequencies_b, [0f32; 256]),
            0.5f32
        );
    }
}
