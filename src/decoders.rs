use std::cmp::Ordering;

use crate::crypto::Crypto;
use crate::util;

pub fn single_byte_xor(ciphertext: &Vec<u8>) -> Result<Vec<u8>, String> {
    (0..=255)
        .map(|b| ciphertext.xor_repeating(&vec![b]))
        .min_by(cmp_frequency_scores)
        .ok_or(String::from("There definitely should be a minimum here."))
}

fn cmp_frequency_scores(x: &Vec<u8>, y: &Vec<u8>) -> Ordering {
    let reference = util::reference_frequencies();
    let x_score = util::score_frequencies(util::calc_frequencies(x), reference);
    let y_score = util::score_frequencies(util::calc_frequencies(y), reference);
    x_score.partial_cmp(&y_score).unwrap_or(Ordering::Equal)
}

pub fn detect_single_byte_xor(v: &Vec<u8>) -> bool {
    let mut reference = util::reference_frequencies();
    let max_ref_index = max_index(&reference.to_vec()).expect("no max?");
    reference.sort_floats();
    let mut frequencies = util::calc_frequencies(v);
    let max_test_index = max_index(&frequencies.to_vec()).expect("no max?");
    frequencies.sort_floats();
    if util::score_frequencies(frequencies, reference) < 0.1f32 {
        let probable_key = (max_ref_index ^ max_test_index) as u8;
        let decoded = v.xor_repeating(&vec![probable_key]);
        String::from_utf8(decoded).is_ok()
    } else {
        false
    }
}

fn max_index(fs: &Vec<f32>) -> Option<usize> {
    fs.iter()
        .enumerate()
        .max_by(|(_, a), (_, b)| a.total_cmp(b))
        .map(|pair| pair.0)
}
