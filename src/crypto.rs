pub trait Crypto {
    fn xor_repeating(&self, key: &Vec<u8>) -> Vec<u8>;
}

impl Crypto for Vec<u8> {
    fn xor_repeating(&self, key: &Vec<u8>) -> Vec<u8> {
        let lazy_key = key.iter().cycle();
        self.clone()
            .iter()
            .zip(lazy_key)
            .map(|(&x, &k)| x ^ k)
            .collect()
    }
}
