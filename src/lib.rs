#![feature(array_zip)]
#![feature(is_some_and)]
#![feature(sort_floats)]

pub mod exercises {
    pub mod set_one;
}

pub mod crypto;
pub mod decoders;
pub mod pretty;
pub mod util;
