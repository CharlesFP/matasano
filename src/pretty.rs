//TODOS
// - represent padding
pub fn from_hex(hex_str: String) -> Result<Vec<u8>, String> {
    hex_str
        .chars()
        .collect::<Vec<char>>()
        .chunks(2)
        .map(|pair| match pair {
            [hi, lo] => {
                let hi_digit = parse_hex_digit(*hi).map(|x| x * 16);
                let lo_digit = parse_hex_digit(*lo);
                hi_digit.and_then(|h| lo_digit.map(|l| h + l))
            }
            _ => Err(String::from(
                "I don't currently want to deal with partial bytes.",
            )),
        })
        .collect()
}

pub fn to_hex(bytes: Vec<u8>) -> Result<String, String> {
    bytes
        .iter()
        .map(|&b| {
            vec![b / 16, b % 16]
                .iter()
                .map(|&n| to_hex_digit(n))
                .collect::<Result<String, String>>()
        })
        .collect::<Result<String, String>>()
}

// char has a function for this https://doc.rust-lang.org/std/primitive.char.html#method.to_digit
fn to_hex_digit(n: u8) -> Result<char, String> {
    match n {
        0..=9 => Ok(('0' as u8 + n) as char),
        10..=15 => Ok(('A' as u8 + n - 10) as char),
        _ => Err(String::from(format!("There isn't a hex digit for {n}."))),
    }
}

// char has a function for this https://doc.rust-lang.org/std/primitive.char.html#method.from_digit
fn parse_hex_digit(c: char) -> Result<u8, String> {
    match c {
        '0'..='9' => Ok(c as u8 - '0' as u8),
        'a'..='f' => Ok(c as u8 - 'a' as u8 + 10),
        'A'..='F' => Ok(c as u8 - 'A' as u8 + 10),
        _ => Err(String::from(format!("{c} is not a hex digit"))),
    }
}

pub fn from_b64(b64_str: String) -> Result<Vec<u8>, String> {
    // Assumes good formatting with '=' only at the end
    let padding_size = b64_str.matches('=').count().saturating_sub(1);

    let tmp = b64_str
        .chars()
        .map(|c| parse_b64_digit(c))
        .collect::<Result<Vec<u8>, String>>()?;

    tmp.chunks(4)
        .map(|chunk| match chunk {
            [a, b, c, d] => Ok(vec![(a << 2) | (b >> 4), (b << 4) | (c >> 2), (c << 6) | d]),
            _ => Err(String::from("base64 String is padded wrong.")),
        })
        .collect::<Result<Vec<Vec<u8>>, String>>()
        .map(|r| r.concat())
        .map(|mut v| {
            v.truncate(v.len() - padding_size);
            v
        })
}

pub fn to_b64(bytes: Vec<u8>) -> Result<String, String> {
    let mut our_bytes = bytes.clone();
    let padding_size = (bytes.len() * 2) % 3;
    for _ in 0..padding_size {
        our_bytes.push(0);
    }
    let six_bits: u32 = 0b111111;
    let mut str: String = our_bytes
        .chunks(3)
        .map(|chunk| {
            let triplet: Result<String, String> = match chunk {
                [a, b, c] => {
                    let collected: u32 = (*a as u32) << 16 | (*b as u32) << 8 | *c as u32;
                    vec![
                        (collected >> 18 & six_bits) as u8,
                        (collected >> 12 & six_bits) as u8,
                        (collected >> 6 & six_bits) as u8,
                        (collected & six_bits) as u8,
                    ]
                    .iter()
                    .map(|&d| to_b64_digit(d))
                    .collect::<Result<String, String>>()
                }
                [] => Ok(String::from("")),
                _ => panic!("why would chunks(3) return a chunk with more than 3"),
            };
            triplet
        })
        .collect::<Result<String, String>>()?;

    let padding_range = str.len() - padding_size..str.len();
    str.replace_range(padding_range, "=".repeat(padding_size).as_str());
    Ok(str)
}

fn to_b64_digit(n: u8) -> Result<char, String> {
    match n {
        0..=25 => Ok(('A' as u8 + n) as char),
        26..=51 => Ok(('a' as u8 + n - 26) as char),
        52..=61 => Ok(('0' as u8 + n - 52) as char),
        62 => Ok('+'),
        63 => Ok('/'),
        _ => Err(String::from(format!("There isn't a base64 digit for {n}."))),
    }
}

fn parse_b64_digit(c: char) -> Result<u8, String> {
    match c {
        'A'..='Z' => Ok(c as u8 - 'A' as u8),
        'a'..='z' => Ok(c as u8 - 'a' as u8 + 26),
        '0'..='9' => Ok(c as u8 - '0' as u8 + 52),
        '+' => Ok(62),
        '/' => Ok(63),
        //Padding char
        //if some sicko uses it in the middle of a b64 string I guess we'll treat it as an A
        '=' => Ok(0),
        bad_char => Err(String::from(format!("{bad_char} is not a base64 digit."))),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_hex() -> () {
        assert_eq!(from_hex(String::from("")), Ok(vec![]));
        assert_eq!(from_hex(String::from("00")), Ok(vec![0]));
        assert_eq!(from_hex(String::from("FF")), Ok(vec![255]));
        assert!(from_hex(String::from("GG")).is_err());
        assert!(from_hex(String::from("F")).is_err());
    }

    #[test]
    fn test_to_hex_and_back_again() -> () {
        assert_eq!(from_hex(String::from("")), Ok(vec![]));
        assert!((0..=255).all(|n| to_hex(vec![n])
            .and_then(|h| from_hex(h))
            .is_ok_and(|m| m == vec![n])));
    }

    #[test]
    fn test_from_b64() -> () {
        assert_eq!(from_b64(String::from("")), Ok(vec![]));
        assert_eq!(from_b64(String::from("AAAA")), Ok(vec![0, 0, 0]));
        assert_eq!(from_b64(String::from("////")), Ok(vec![255, 255, 255]));
        assert_eq!(from_b64(String::from("ABCD")), Ok(vec![0, 16, 131]));
        assert_eq!(from_b64(String::from("AAA=")), Ok(vec![0, 0, 0]));
        assert_eq!(from_b64(String::from("AA==")), Ok(vec![0, 0]));
        assert_eq!(from_b64(String::from("A===")), Ok(vec![0]));
        assert!(from_b64(String::from("AAA")).is_err());
    }

    #[test]
    fn test_to_b64_and_back_again() -> () {
        assert_eq!(from_b64(String::from("")), Ok(vec![]));
        assert!((0..(1 << 25 - 1)).step_by(63).all(|n: u32| {
            let hi = (n >> 16) as u8;
            let md = (0b111111 & (n >> 8)) as u8;
            let lo = (0b111111 & n) as u8;
            let v = vec![hi, md, lo];
            to_b64(v.clone()).and_then(from_b64).is_ok_and(|w| w == v)
        }))
    }
}
