use std::io;
use std::io::BufRead;
use std::io::Write;

use crate::crypto::Crypto;
use crate::decoders;
use crate::decoders::detect_single_byte_xor;
use crate::pretty;
use crate::util;

pub fn select(stdin: &mut io::StdinLock, stdout: &mut io::StdoutLock) -> io::Result<()> {
    let mut input = String::new();
    loop {
        stdout
            .write_all(b"\nPart: (q to exit): ")
            .and(stdout.flush())?;
        input.clear();
        stdin.read_line(&mut input)?;

        if input.starts_with("1") {
            part_one(stdin, stdout)?
        } else if input.starts_with("2") {
            part_two(stdin, stdout)?
        } else if input.starts_with("3") {
            part_three(stdin, stdout)?
        } else if input.starts_with("4") {
            part_four(stdin, stdout)?
        } else if input.starts_with("5") {
            part_five(stdin, stdout)?
        } else if input.starts_with("q") {
            break;
        } else {
            stdout
                .write_all(b"\nI haven't done that set yet.")
                .and(stdout.flush())?
        }
    }
    Ok(())
}

fn part_one(stdin: &mut io::StdinLock, stdout: &mut io::StdoutLock) -> io::Result<()> {
    let mut hex_input = String::new();
    stdout
        .write_all(b"\nEnter some hex: ")
        .and(stdout.flush())?;
    stdin.read_line(&mut hex_input)?;

    match pretty::from_hex(String::from(hex_input.trim_end())).and_then(pretty::to_b64) {
        Ok(b64_str) => stdout
            .write_all(b"\nIn base64, that's: ")
            .and(stdout.write_all(b64_str.as_bytes()))
            .and(stdout.flush())?,
        Err(msg) => stdout.write_all(msg.as_bytes()).and(stdout.flush())?,
    }

    let mut b64_input = String::new();
    stdout
        .write_all(b"\nNow enter some base64: ")
        .and(stdout.flush())?;
    stdin.read_line(&mut b64_input)?;

    match pretty::from_b64(String::from(b64_input.trim_end())).and_then(pretty::to_hex) {
        Ok(hex_str) => {
            stdout
                .write_all("\nIn hex, that's: ".as_bytes())
                .and(stdout.write_all(hex_str.as_bytes()))
                .and(stdout.flush())?;
        }
        Err(msg) => stdout.write_all(msg.as_bytes()).and(stdout.flush())?,
    }
    Ok(())
}

fn part_two(stdin: &mut io::StdinLock, stdout: &mut io::StdoutLock) -> io::Result<()> {
    let mut input_xs = String::new();
    let mut input_ys = String::new();
    stdout
        .write_all(b"\nLet's have some hex: ")
        .and(stdout.flush())?;
    stdin.read_line(&mut input_xs)?;
    stdout
        .write_all(b"\nLet's have some more: ")
        .and(stdout.flush())?;
    stdin.read_line(&mut input_ys)?;

    let x_bytes = pretty::from_hex(String::from(input_xs.trim_end()));
    let y_bytes = pretty::from_hex(String::from(input_ys.trim_end()));
    match x_bytes.and_then(|xs| y_bytes.and_then(|ys| util::xor(xs, ys).and_then(pretty::to_hex))) {
        Ok(hex_out) => stdout
            .write_all(b"\nxor'd together, those make: ")
            .and(stdout.write_all(hex_out.as_bytes()))
            .and(stdout.flush())?,
        Err(msg) => stdout.write_all(msg.as_bytes()).and(stdout.flush())?,
    }

    Ok(())
}

fn part_three(stdin: &mut io::StdinLock, stdout: &mut io::StdoutLock) -> io::Result<()> {
    let mut cipher_text = String::new();

    stdout
        .write_all(b"\nLet's have some hex: ")
        .and(stdout.flush())?;
    stdin.read_line(&mut cipher_text)?;

    let cipher_bytes = pretty::from_hex(String::from(cipher_text.trim_end()));
    let decoded_bytes = cipher_bytes.and_then(|bytes| decoders::single_byte_xor(&bytes));
    match decoded_bytes {
        Ok(s) => stdout
            .write_all(b"\nDecoded, I think that says: ")
            .and(stdout.write_all(&s))
            .and(stdout.flush())?,
        Err(msg) => stdout.write_all(msg.as_bytes()).and(stdout.flush())?,
    }

    Ok(())
}

fn part_four(stdin: &mut io::StdinLock, stdout: &mut io::StdoutLock) -> io::Result<()> {
    let mut candidate_hex = String::new();

    stdout
        .write_all(b"\nLet's have some hex: ")
        .and(stdout.flush())?;
    stdin.read_line(&mut candidate_hex)?;

    let candidate_bytes = pretty::from_hex(String::from(candidate_hex.trim_end()))
        .map(|bytes| detect_single_byte_xor(&bytes));

    match candidate_bytes {
        Ok(true) => stdout
            .write_all(b"\nThat looks encoded with a single byte xor cipher.")
            .and(stdout.flush())?,
        Ok(false) => stdout
            .write_all(b"\nI don't recognize that cipher.")
            .and(stdout.flush())?,
        Err(msg) => stdout.write_all(msg.as_bytes()).and(stdout.flush())?,
    }

    Ok(())
}

fn part_five(stdin: &mut io::StdinLock, stdout: &mut io::StdoutLock) -> io::Result<()> {
    let mut key = String::new();
    let mut plaintext = String::new();
    stdout.write_all(b"\nEnter a key: ").and(stdout.flush())?;
    stdin.read_line(&mut key)?;
    stdout
        .write_all(b"\nEnter plaintext: ")
        .and(stdout.flush())?;
    stdin.read_line(&mut plaintext)?;

    let key_bytes = key.trim_end().as_bytes().to_vec();
    let plain_bytes = plaintext.trim_end().as_bytes().to_vec();
    match pretty::to_hex(plain_bytes.xor_repeating(&key_bytes)) {
        Ok(hex_out) => stdout
            .write_all(b"\nYour encrypted and hex-encoded output is: ")
            .and(stdout.write_all(hex_out.as_bytes()))
            .and(stdout.flush())?,
        Err(msg) => stdout.write_all(msg.as_bytes()).and(stdout.flush())?,
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::io;
    use std::io::BufRead;
    use std::path;

    use crate::crypto::Crypto;
    use crate::decoders;
    use crate::pretty;
    use crate::util;

    #[test]
    fn test_part_one() {
        let b64_str =
            String::from("SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t");
        let hex_str = String::from("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d");

        let hex_bytes = pretty::from_hex(hex_str.clone()).unwrap();
        let b64_bytes = pretty::from_b64(b64_str.clone()).unwrap();
        assert_eq!(hex_bytes, b64_bytes);

        let hex_out = pretty::to_hex(b64_bytes).unwrap();
        let b64_out = pretty::to_b64(hex_bytes).unwrap();
        assert_eq!(hex_str.to_uppercase(), hex_out.to_uppercase());
        assert_eq!(b64_str, b64_out);
    }

    #[test]
    fn test_part_two() {
        let hexxs = String::from("1c0111001f010100061a024b53535009181c");
        let hexys = String::from("686974207468652062756c6c277320657965");
        let hexpected = String::from("746865206B696420646F6E277420706C6179");

        let xs = pretty::from_hex(hexxs).unwrap();
        let ys = pretty::from_hex(hexys).unwrap();
        let hexor = util::xor(xs, ys).and_then(pretty::to_hex).unwrap();

        assert_eq!(hexor, hexpected);
    }

    #[test]
    fn test_part_three() -> () {
        let answer = String::from("Cooking MC's like a pound of bacon");
        let input_hex =
            String::from("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736");
        let input_bytes = pretty::from_hex(input_hex).unwrap();
        let decoded_bytes = decoders::single_byte_xor(&input_bytes).unwrap();
        assert_eq!(decoded_bytes, answer.as_bytes());
    }

    #[test]
    fn test_part_four() -> () {
        let test_path = path::Path::new(".")
            .join("resources")
            .join("set1_part4.txt");
        let test_file = fs::File::open(&test_path).expect("missing reference materials");
        let guesses: Vec<bool> = io::BufReader::new(test_file)
            .lines()
            .map(|line| {
                let line_hex = line.expect("Failed to read line");
                let bytes = pretty::from_hex(line_hex).expect("Invalid hex in test.");
                decoders::detect_single_byte_xor(&bytes)
            })
            .collect();
        let mut answers = [false; 327];
        answers[170] = true;
        let mistakes: f32 = guesses
            .iter()
            .zip(answers.iter())
            .map(|(&g, &a)| {
                if a && !g {
                    10
                } else if g && !a {
                    1
                } else {
                    0
                }
            })
            .sum::<u32>() as f32
            / answers.len() as f32;
        assert!(mistakes < 0.01f32);
    }

    #[test]
    fn test_part_five() -> () {
        let test_bytes = String::from(
            "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal",
        )
        .as_bytes()
        .to_vec();
        let key = String::from("ICE").as_bytes().to_vec();
        let encoded = test_bytes.xor_repeating(&key);
        let expected_bytes = pretty::from_hex(String::from("0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f")).unwrap();
        assert_eq!(encoded, expected_bytes);
    }
}
